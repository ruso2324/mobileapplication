package com.example.myapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var editTextEmailAddress: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextRepeatPassword: EditText
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        registerListener ()

    }
    private fun init(){
        editTextEmailAddress = findViewById(R.id.editTextEmailAddress)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextRepeatPassword = findViewById(R.id.editTextRepeatPassword)
        button = findViewById(R.id.button)


    }
    private fun registerListener() {
        button.setOnClickListener {
            val email = editTextEmailAddress.text.toString()
            val password = editTextPassword.text.toString()
            val repeatPassword = editTextRepeatPassword.text.toString()

            if (email.isEmpty()) {
                Toast.makeText(this, "Field is empty", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }
            if (password.isEmpty()) {
                Toast.makeText(this, "Field is empty", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (repeatPassword.isEmpty()) {
                Toast.makeText(this, "Field is empty", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (password.length<8) {
                Toast.makeText(this, "The password must contain at least 8 character", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (password != repeatPassword) {
                Toast.makeText(this, "Passwords don't match", Toast.LENGTH_SHORT).show()
            }else if (password != repeatPassword && repeatPassword.isEmpty()) {
                Toast.makeText(this, "Field is empty", Toast.LENGTH_SHORT).show()
            }
            if (password==repeatPassword)  {
                FirebaseAuth.getInstance()
                    .createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if(task.isSuccessful) {
                            Toast.makeText(this, "Registration Successful", Toast.LENGTH_SHORT).show()

                        } else {
                            Toast.makeText(this, "Registration wasn't successful", Toast.LENGTH_SHORT).show()
                        }

                    }
            }




        }

    }
}